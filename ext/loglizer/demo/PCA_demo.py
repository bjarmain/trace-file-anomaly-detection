#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
import pandas as pd
from loglizer.models import PCA
from loglizer import dataloader, preprocessing

import plotly.express as px
import plotly.graph_objects as go


def SPE_plot_Markers(model, x_test):
    num = model.get_SPE(x_test)
    df = pd.DataFrame(data=num, columns=['Item No', 'SPE'])
    series = df['SPE']
    indices = [i for i in range(series.shape[0]) if series[i] >= model.threshold] # find_peaks(series, threshold=model.threshold)[0]
    fig = go.Figure()
    fig.update_layout(title="SPE Plot",
            font=dict(
            size=18
    ))
    fig.add_trace(go.Scatter(y=series, 
            mode='lines+markers', 
            name='Original Plot'))
    fig.add_trace(go.Scatter(
            x=indices, 
            y=[series[j] for j in indices], 
            mode='markers', 
            marker=dict(
                size=8, 
                color='red', 
                symbol='cross'), 
            name='Detected Peaks'))
    fig.show()


def scree_plot(model):
    data = pd.DataFrame(data={"Principal Component":["PC " + str(n + 1) for n in range(model.normal_comonents)], "Explained Variance":[100 * model.sigma[n] / model.total_variance for n in range(model.normal_comonents)]})
    fig = px.bar(data, x='Principal Component', y='Explained Variance', title='Scree Plot')
    fig.show()



struct_log = '../../../../data/HDFS/HDFS_100k.log_structured.csv' # The structured log file
label_file = '../../../../data/HDFS/anomaly_label.csv' # The anomaly label file

if __name__ == '__main__':
    (x_train, y_train), (x_test, y_test) = dataloader.load_HDFS(struct_log,
                                                                label_file=label_file,
                                                                window='session', 
                                                                train_ratio=0.5,
                                                                split_type='uniform')
    feature_extractor = preprocessing.FeatureExtractor()
    x_train, x_df = feature_extractor.fit_transform(x_train, term_weighting='tf-idf', 
                                              normalization='zero-mean')
    x_test = feature_extractor.transform(x_test)

    model = PCA()
    model.fit(x_train)
    
    fig = px.scatter(x_train, x=0, y=1)
    fig.show()

    print('Train validation:')
    precision, recall, f1 = model.evaluate(x_train, y_train)
    
    print('Test validation:')
    precision, recall, f1 = model.evaluate(x_test, y_test)

    SPE_plot_Markers(model, x_train)
    scree_plot(model)
