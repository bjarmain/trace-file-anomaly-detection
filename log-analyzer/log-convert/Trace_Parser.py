#!/usr/bin/env python3
import sys
from pathlib import Path
import pandas as pd
from datetime import datetime
from collections import defaultdict
import re
import os


class LogParser():
    def __init__(self, indir, outdir, log_format,
                 rex=[], keep_para=True, max_lines = None):
        self.logformat = log_format
        self.savePath = outdir
        self.rex = rex
        self.df_log = None
        self.logname = indir
        self.merged_events = []
        self.bins = defaultdict(dict)
        self.keep_para = keep_para
        self.max_lines = max_lines

    def parse(self):
        start_time = datetime.now()
        
        print('Parsing file: ' + self.logname)
        print("1/3 loading...")
        self.load_data()
        print("2/3 fixing data...")
        self.fix_data()
        print("3/3 dumping...")
        self.dump()
        
        print('Parsing done. [Time taken: {!s}]'.format(datetime.now() - start_time))

    def load_data(self):
        headers, regex = self.generate_logformat_regex(self.logformat)
        self.df_log = self.log_to_dataframe(self.logname, regex, headers, self.logformat)

    def fix_data(self):
        self.df_log["File"] = self.df_log["File"].str.replace(r'__[a-z_-]+\.py', '.py', regex=True)
        self.df_log["Location"] = self.df_log["Thread"]+"--->"+self.df_log["File"]+"--->"+self.df_log["Line"];
        # self.df_log["Location"] = self.df_log["Location"].str.replace(r'__[a-z_-]+\.py', '.py', regex=True)

    def generate_logformat_regex(self, logformat):
        '''
        Function to generate regular expression to split log messages

        '''
        print("generating regex")
        headers = []
        splitters = re.split(r'(<[^<>]+>)', logformat)
        regex = ''
        for k in range(len(splitters)):
            if k % 2 == 0:
                splitter = re.sub(' +', '\\\s+', splitters[k])
                regex += splitter
            else:
                header = splitters[k].strip('<').strip('>')
                if k+2 < (len(splitters)):
                    regex += '(?P<%s>\\S*)' % header
                else:
                    regex += '(?P<%s>.*)' % header
                headers.append(header)
        regex = re.compile('^' + regex + '$')
        return headers, regex

    def log_to_dataframe(self, log_file, regex, headers, logformat):
        ''' Function to transform log file to dataframe '''
        log_messages = []
        linecount = 0
        print("consuming file")
        with open(log_file, 'r') as fin:
            for line in fin.readlines():
                try:
                    stripped = line.strip()
                    match = regex.search(stripped)
                    if match != None:
                        message = [match.group(header) for header in headers]
                        log_messages.append(message)
                        linecount += 1
                        if linecount >= self.max_lines:
                            break
                except Exception as e:
                    pass
        logdf = pd.DataFrame(log_messages, columns=headers)
        logdf.insert(0, 'LineId', None)
        print("adding line numbers")
        logdf['LineId'] = [i + 1 for i in range(linecount)]
        return logdf

    def dump(self):
        if not os.path.isdir(self.savePath):
            os.makedirs(self.savePath)
        original_name = Path(self.logname).name
        self.df_log.to_csv(os.path.join(self.savePath, original_name + '_struct.csv'), index=False, columns=["Time", "Thread", "File", "Func", "Line", "Location", "Content"])


input_dir     = sys.argv[-2] # The input directory of log file
output_dir    = sys.argv[-1] # The output directory of parsing results
log_format    = '<Time> <File> <Func> <Thread> <Line> \| <Content>' # HDFS log format

print(f"From: {input_dir}")
print(f"To  : {output_dir}")
parser = LogParser(input_dir, output_dir, log_format) #, max_lines=2000)
parser.parse()

