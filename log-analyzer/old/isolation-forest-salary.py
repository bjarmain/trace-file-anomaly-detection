#!/usr/bin/env python3

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest


def main():
   df = pd.read_csv('salary.csv')
   print (df.head(10))
   print()
   model=IsolationForest(n_estimators=50, max_samples='auto', contamination=float(0.1),max_features=1.0)
   model.fit(df[['salary']])
   df['scores']=model.decision_function(df[['salary']])
   df['anomaly']=model.predict(df[['salary']])
   print (df.head(20))
   print()
   anomaly=df.loc[df['anomaly']==-1]
   anomaly_index=list(anomaly.index)
   print(anomaly)
   print()
   outliers_counter = len(df[df['salary'] > 99999])
   print(outliers_counter)
   print("Accuracy percentage:", 100*list(df['anomaly']).count(-1)/(outliers_counter))
   print()

main()