#!/usr/bin/env bash

# To use:
#./generate__errors.sh <python-script> <rel-out-path> <full-in-path>
#
# About:
# This is optimized for capturing trace files using a script with an induced anomaly.
# It will capture trace files run using <python-script>
# with input binary files found in <full-in-path> 
# and stores the traces in <rel-out-path> named reflecting the test conditions.

(
mkdir -p $2/raw
cd $2
test_list=`ls -Srd $3/*`
for test in $test_list
do
   in_file=`basename $test`
   script_name=`basename $1`
   echo $in_file
   (echo "Testing $1 against: $test (trace)") |tee -a gen_time.txt
   (set -x; (time $1 ${test} --trace) 2>> gen_time.txt )
   (set -x; for f in $2/raw/*.txt; do mv -- "$f" "${f}___${script_name}__.log"; done )
   (echo "====================================================") |tee -a gen_time.txt
   (set -x; rm -rf *$in_file )
done
)


