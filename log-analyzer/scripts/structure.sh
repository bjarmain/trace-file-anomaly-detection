#!/usr/bin/env bash

# To use:
#./structure.sh <python-script> <rel-out-path> <full-in-path> 
#
# About:
# This is for launcing a python script used to convert a raw trace file into a structured CSV format.
# The conversion will be done using <python-script>
# with all raw trace files found in <full-in-path> 
# and stores the resulting CSV files in <rel-out-path>.

(
mkdir -p $2/structured
cd $2
rm structure_time.txt
test_list=`ls -Srd $3/*`
for test in $test_list
do
   (echo "Converting to Structured: $test") |tee -a structure_time.txt
   (set -x; (time python $1 $test $PWD/structured) 2>> structure_time.txt )
   (echo "====================================================") |tee -a structure_time.txt
done
)

