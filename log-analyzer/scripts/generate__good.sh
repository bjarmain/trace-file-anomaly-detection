#!/usr/bin/env bash

# To use:
#./generate__good.sh <python-script> <rel-out-path> <full-in-path>
#
# About:
# This is optimized for capturing trace files using a script without an induced anomaly.
# It will capture trace files run using <python-script> 
# with input binary files found in <full-in-path> 
# and stores the traces in <rel-out-path> named reflecting the test conditions.

(
mkdir -p $2/raw
cd $2
rm gen_time.txt
test_list=`ls -Srd $3/*`
for test in $test_list
do
   in_file=`basename $test`
   echo $in_file
   (echo "Testing against: $test (NO trace)") |tee -a gen_time.txt
   (set -x; (time $1 $test ) 2>> gen_time.txt )
   (set -x; rm -rf *$in_file )
   (echo "____________________________________________________") |tee -a gen_time.txt
   (echo "Testing against: $test (trace)") |tee -a gen_time.txt
   (set -x; (time $1 $test --trace) 2>> gen_time.txt )
   (echo "====================================================") |tee -a gen_time.txt
   (set -x; rm -rf *$in_file )
done
)

