#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import plotly.express as px
import pandas as pd

# sys.path.append('/home/bryan/research-project/research-project/ext/loglizer')
from loglizer.models import PCA
from loglizer import dataloader, preprocessing

struct_log = '/home/bryan/research-project/orig-ext/loglizer/data/HDFS/HDFS_100k.log_structured.csv' # The structured log file
#struct_log = '../../../data/HDFS/HDFS_1k.log_structured.csv' # The structured log file
label_file = '/home/bryan/research-project/orig-ext/loglizer/data/HDFS/anomaly_label.csv' # The anomaly label file

if __name__ == '__main__':
    (x_train, y_train), (xo_test, y_test) = dataloader.load_HDFS(struct_log,
                                                                label_file=label_file,
                                                                window='session',   #only session is supported
                                                                train_ratio=0.5,
                                                                split_type='uniform')
    #feature extraction
    feature_extractor = preprocessing.FeatureExtractor()
    x_train,x_df = feature_extractor.fit_transform(x_train, term_weighting='tf-idf', 
                                              normalization='zero-mean')

    model = PCA()
    model.fit(x_train)

    print('Train validation:')
    precision, recall, f1 = model.evaluate(x_train, y_train)
    
    print('Test validation:')
    x_test = feature_extractor.transform(xo_test)
    precision, recall, f1 = model.evaluate(x_test, y_test)

    pred = model.predict(x_test)
    
    num = model.get_SPE(x_test)
    df = pd.DataFrame(data = num, columns=['Item No','SPE'])
    fig = px.line(df, x="Item No", y="SPE", title='SPE-Chart')
    fig.show()
    
    exit()

    
    print("\n\nTrain: ", y_train.size)
    for i in range(y_train.size):
        if i % 20 == 0:
            print()
        print(y_train[i], end = ", ")


    print("\n\nTest: ", y_test.size, xo_test.size)
    for i in range(y_test.size):
        # if i % 20 == 0:
            # print()
        # print(int(pred[i]), end = ", ")
        # print(int(y_test[i]), end = ", ")
        res = (int(pred[i])+int(y_test[i]))*(int(y_test[i]*-1)+(int(y_test[i]==0)))
        if(res == -2):
            print(int(pred[i]),y_test[i],xo_test[i])
        # print((int(pred[i])+int(y_test[i]))*int(y_test[i]*-1), end = ", ")

