#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
from loglizer.models import InvariantsMiner
from loglizer import dataloader, preprocessing


# struct_log = '../data/HDFS/HDFS_100k.log_structured.csv' # The structured log file
# label_file = '../data/HDFS/anomaly_label.csv' # The anomaly label file
struct_log = '../../../data/HDFS/HDFS_100k.log_structured.csv' # The structured log file
label_file = '../../../data/HDFS/anomaly_label.csv' # The anomaly label file
epsilon = 0.5 # threshold for estimating invariant space

if __name__ == '__main__':
    (x_train, y_train), (x_test, y_test) = dataloader.load_HDFS(struct_log,
                                                                label_file=label_file,
                                                                window='session', 
                                                                train_ratio=0.5,
                                                                split_type='sequential')
    #feature extract
    feature_extractor = preprocessing.FeatureExtractor()
    x_train, x_df = feature_extractor.fit_transform(x_train)

    #train
    model = InvariantsMiner(epsilon=epsilon)
    model.fit(x_train)

    #test
    print('Train validation:')
    precision, recall, f1 = model.evaluate(x_train, y_train)
    
    print('Test validation:')
    x_test = feature_extractor.transform(x_test)
    precision, recall, f1 = model.evaluate(x_test, y_test)

