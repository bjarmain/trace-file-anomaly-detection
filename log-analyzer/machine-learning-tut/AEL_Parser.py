#!/usr/bin/env python
import sys
sys.path.append('/home/bryan/research-project/orig-ext/logparser/')
from logparser import AEL

input_dir     = '../../../data' # The input directory of log file
output_dir    = 'result/' # The output directory of parsing results
log_file      = 'trace2.txt' # The input log file name
log_format    = '<Time> <File> <Func> <Thread> <Line> \| <Content>' # HDFS log format
minEventCount = 2 # The minimum number of events in a bin
merge_percent = 0.5 # The percentage of different tokens
regex         = [] # Regular expression list for optional preprocessing (default: [])

parser = AEL.LogParser(input_dir, output_dir, log_format,
                       minEventCount=minEventCount, merge_percent=merge_percent)
parser.parse(log_file)

