#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
from loglizer.models import IsolationForest
from loglizer import dataloader, preprocessing


struct_log = '/home/bryan/research-project/orig-ext/loglizer/data/HDFS/HDFS_100k.log_structured.csv' # The structured log file
label_file = '/home/bryan/research-project/orig-ext/loglizer/data/HDFS/anomaly_label.csv' # The anomaly label file
anomaly_ratio = 0.03 # Estimate the ratio of anomaly samples in the data

if __name__ == '__main__':
    (x_train, y_train), (x_test, y_test) = dataloader.load_HDFS(struct_log,
                                                                label_file=label_file,
                                                                window='session', 
                                                                train_ratio=0.5,
                                                                split_type='uniform')
    #feature extract
    feature_extractor = preprocessing.FeatureExtractor()
    x_train,X_df = feature_extractor.fit_transform(x_train)

    #train
    model = IsolationForest(contamination=anomaly_ratio)
    model.fit(x_train)

    #test
    print('Train validation:')
    precision, recall, f1 = model.evaluate(x_train, y_train)
    
    print('Test validation:')
    x_test = feature_extractor.transform(x_test)
    precision, recall, f1 = model.evaluate(x_test, y_test)

