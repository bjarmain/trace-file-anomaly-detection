#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import math
import pprint
import pandas as pd
import numpy as np
from loglizer.models import PCA
from loglizer.models import IsolationForest
from loglizer.models import InvariantsMiner
from sklearn.metrics import precision_recall_fscore_support


from loglizer import preprocessing
from collections import OrderedDict,Counter

import plotly.express as px
import plotly.graph_objects as go

import time
import functools
import csv


def time_this(func=None, friendly_name=None):
    def time_dec(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start = time.perf_counter()
            result = func(*args, **kwargs)
            if friendly_name:
                description = friendly_name
            else:
                description = func.__name__
            time_stats.append({'Model':context_model
                               ,'File':context_file
                               ,'Method':f'{method["friendly name"]})'
                               ,'Function Timed': func.__name__
                               ,'Timed Action':description
                               ,'Timed Action (method)':f'{description} ({method["friendly name"]})'
                               ,'Timed Action (model)':f'{description} ({context_model})'
                               ,'Timed Action (model, method)':f'{description} ({context_model}, {method["friendly name"]})'
                               ,'Time Taken (s)':time.perf_counter() - start
                               ,'Units':"seconds"})
            return result
        return wrapper
    return time_dec(func) if callable(func) else time_dec

# feature extraction functions
def load_trace_timeslice(log_file, is_limited):
    base_file=os.path.basename(log_file)
    print(f"Extracting Data as execution-slices from '{base_file}'")
    trace_data = pd.read_csv(log_file, engine='c',
                na_filter=False, memory_map=True)    #read csv into memory (fast)
    
    data_dict = OrderedDict()
    raw_lines = trace_data.shape[0]

    num_segments = int(raw_lines / timeslice_size)
    
    col_pos = 0
    seg_num = 0
    line_num = 0
    for idx, row in trace_data.iterrows(): #for each blk_ gather all Event-Ids
        seg_name = "seg_"+str(seg_num)
        if not seg_name in data_dict:
            data_dict[seg_name] = []
        data_dict[seg_name].append(row['Location'])
        col_pos = col_pos + 1
        if col_pos >= timeslice_size:
            data_dict[seg_name]=Counter(data_dict[seg_name])
            col_pos = 0
            seg_num = seg_num + 1
        line_num += 1
        if is_limited and line_num > max_limited_lines:
            break
    if col_pos != 0:
        data_dict[seg_name]=Counter(data_dict[seg_name])
    data_df = pd.DataFrame(list(data_dict.items()), columns=['seg', 'EventSequence']) 
    return data_df


def load_trace_forks(log_file, is_limited):
    base_file=os.path.basename(log_file)
    print(f"Extracting Data as Code Forks from '{base_file}'")
    trace_data = pd.read_csv(log_file, engine='c',
                na_filter=False, memory_map=True)    #read csv into memory (fast)
    
    raw_lines = trace_data.shape[0]
    num_cols = 2 #int(math.sqrt(raw_lines))
    # if num_cols > 1000:
    #     num_cols = 1000
    num_segments = int(raw_lines / num_cols)
    
    last_loc = {}
    grid_rows = OrderedDict()
    col_pos = 0
    line_num = 0
    for idx, row in trace_data.iterrows(): #for each blk_ gather all Event-Ids
        region_name=row['File']+"->"+row['Func']
        region = row['Location'] # +"::"+row['Content']  #uncomment for further context
        if not region_name in last_loc:
            last_loc[region_name] = '[origin]->'+region_name
        if not last_loc[region_name] in grid_rows:
            grid_rows[last_loc[region_name]]=Counter()
            
        if region != last_loc[region_name]:
            grid_rows[last_loc[region_name]][last_loc[region_name]] += 1
            grid_rows[last_loc[region_name]][region] += 1
            last_loc[region_name] = region
        line_num += 1
        if is_limited and line_num > max_limited_lines: 
            break
    
    single_path_stats = 0
    fork_path_stats = 0
    data_dict = OrderedDict()
    data_summary = OrderedDict()
    for row_key in grid_rows:
        row = grid_rows[row_key]
        if len(row.keys()) > 2: #ignore code with single path
            # items=sorted(row.most_common(),key=lambda k:k[1])
            items=[(k,v)for k,v in row.items()]
            if True:  #items[-1][1] >= items[0][1]: # > :unbalanced multi-forks, >= all multi-forks 
                fork_path_stats +=  1
                key = "[{0:d}/{1:d}/{2:d}]{3}".format(items[0][1],items[1][1],items[-1][1],row_key)
                # sublists = [[fork[0] for repeated in range(fork[1])] for fork in items]
                # data_dict[key]=[item for sublist in sublists for item in sublist]  #flatten
                data_dict[key]=items
                data_summary[key] = items[0][0]+" /// "+items[-1][0]
        else:
            single_path_stats += 1
    print("Load Stats = %d/%d  Forks vs single-path"%(fork_path_stats,single_path_stats))
    
    data_df = pd.DataFrame(list(data_dict.items()), columns=['seg', 'EventSequence']) 
    return data_df


def load_trace_call_grid(log_file, is_limited):
    base_file=os.path.basename(log_file)
    print(f"Extracting Data as Call Grid matrix from '{base_file}'")
    trace_data = pd.read_csv(log_file, engine='c',
                na_filter=False, memory_map=True)    #read csv into memory (fast)
    line_num = 0
    callers_origin="ext"
    call_grid = OrderedDict()
    for idx, row in trace_data.iterrows(): #for each blk_ gather all Event-Ids
        func_called=row['File']+"->"+row['Func']
        if not callers_origin in call_grid:
            call_grid[callers_origin]=Counter()
        if func_called != callers_origin:
            call_grid[callers_origin][func_called] += 1
            callers_origin = func_called
        line_num += 1
        if is_limited and line_num > max_limited_lines: 
            break

    data_dict=call_grid
    # data_dict = OrderedDict()
    # for row_key in call_grid:
        # called_funcs = [[k for i in range (v)]for k,v in call_grid[row_key].items()]
        # data_dict[row_key]=[item for called_func in called_funcs for item in called_func]  #flatten
                
    data_df = pd.DataFrame(list(data_dict.items()), columns=['seg', 'EventSequence']) 
    return data_df
    
def load_trace(log_file):
    is_limited_for_speed = "max_limited_lines" in globals()
    
    if method['short name'] == "use_exec_slice":
        trace_data = load_trace_timeslice(log_file, is_limited_for_speed)
    elif method['short name'] == "use_trace_forks":
        trace_data = load_trace_forks(log_file, is_limited_for_speed)
    elif method['short name'] == "use_call_grid":
        trace_data = load_trace_call_grid(log_file, is_limited_for_speed)
    else:
        raise Exception("no methods selected")
    return trace_data

@time_this(friendly_name=f"Load training data")
def load_trining_data(directory, training_files):
    full_df = {}
    for my_file in training_files:
        my_path = os.path.join(directory,my_file)
        full_df[my_file] = load_trace(my_path)
    return full_df

@time_this(friendly_name=f"Load test data")
def load_test_data(training_file):
    return load_trace(training_file)

#visualization
def plot_scatter_for(x_test):
    fig = px.scatter(x_test, x=0, y=1)
    fig.show()

def graph_feature_grid(x_train, x_df):
    fig = px.imshow(x_df, labels=dict(x="A line of code", y="segment", color="frequency"), 
        x=x_df.columns, 
        y=[t for t in range(x_train.shape[0])])
    fig.update_xaxes(side="top")
    fig.show()

def graph_train_grid(x_train, x_df):
    fig = px.imshow(x_train, labels=dict(x="B line of code", y="segment", color="frequency"), 
        x=x_df.columns, 
        y=[t for t in range(x_train.shape[0])])
    fig.update_xaxes(side="top")
    fig.show()


def make_stats_dict(filename, my_method, my_model):
    results = OrderedDict()
    results["full name"] = f'{filename}:{my_method}:{my_model}'
    results["file"] = f'{filename}'
    results["method"] = f'{my_method}'
    results["my_model"] = f'{my_model}'
    results["total tests"] = 0
    results["expected positives"] = 0
    results["actual positives"] = 0
    results["true positives"] = 0
    results["false positives"] = 0
    results["expected negatives"] = 0
    results["actual negatives"] = 0
    results["true negatives"] = 0
    results["false negatives"] = 0
    results["Precision"] = 0
    results["Recall"] = 0
    results["F-score"] = 0
    return results


def update_results_with(file_name, my_method, my_model, y_test, y_test_expect):
    results = make_stats_dict(file_name, my_method, my_model)    
    num_found = 0
    n_expected = len(y_test_expect.index)
    n_y_test = y_test.shape[0]
    for i in range(n_y_test):
        expected = 0
        if i < n_expected:
            try:
                expected = int(y_test_expect["anomaly"][i])
            except:
                print(f"!!!!!!!!!!!!!!!!!!!--EXCEPTION-- couldn't get expected annomaly in {file_name} for {i} expected {n_expected} -- resorting to {expected}")
        results["total tests"] += 1
        if 0 != int(y_test[i]):
            results["actual positives"] += 1
        else:
            results["actual negatives"] += 1
        if 1 == expected:
            results["expected positives"] += 1
            if expected == int(y_test[i]):
                results["true positives"] += 1
            else:
                results["false negatives"] += 1
        else:
            results["expected negatives"] += 1
            if expected == int(y_test[i]):
                results["true negatives"] += 1
            else:
                results["false positives"] += 1
        if (i+1) % 100 == 0:
            print() # if y_test[i] != 0:
        # counts = Counter(trace_data["EventSequence"].values[i])
        print(int(y_test[i]),end='')#,trace_data["seg"].values[i],"->",[{k:v} for (k,v) in counts.items()])
        if int(y_test[i]) == 1:
            num_found = num_found + 1
    
    print()
    print("Shape:", y_test.shape, "Size:", y_test.size, "Anomalies:", num_found)
    
    if y_test.size == n_expected:
        precision, recall, f1, _ = precision_recall_fscore_support(y_test_expect["anomaly"], y_test, average='binary') #, zero_division = 1)
    else:
        precision, recall, f1 = (0, 0, 0)
    print('Precision: {:.3f}, Recall: {:.3f}, F1-measure: {:.3f}'.format(precision, recall, f1))
    results["Precision"] = precision    
    results["Recall"] = recall    
    results["F-score"] = f1    

    global results_stats
    results_stats = results_stats.append(results, ignore_index=True)
    print(results)
    print()
    return results


def uppdate_meta_results_with(my_stat, name, results, is_y_test_file):
    global meta_stats
    if not my_stat in meta_stats:
        meta_stats[my_stat] = {}
    if not name in meta_stats[my_stat]:
        meta_stats[my_stat][name] = {}
        meta_stats[my_stat][name]["expected"] = []
        meta_stats[my_stat][name]["actual"] = []
    meta_stats[my_stat][name]["expected"].append(int(is_y_test_file))  #if the expected file exists, an anomaly should have been found
    meta_stats[my_stat][name]["actual"].append(int(results["actual positives"] != 0))



def show_predicted_anomalies(y_test, trace_data):
    expected_anomalies_file = f'{log_with_possible_errors}(({method["short name"]})).csv'
    expected_anomalies_path = os.path.join(expect_results_dir, "anomalies", expected_anomalies_file)
    if os.path.exists(expected_anomalies_path):
        y_test_expect = pd.read_csv(expected_anomalies_path, engine='c',
                na_filter=False, memory_map=True)    #read csv into memory (fast)
        is_y_test_file = True
    else:
        no_annomalies = [0 for _ in range (len(y_test))]
        y_test_expect = pd.DataFrame(no_annomalies, columns = ["anomaly"])
        y_test_expect ["context"] = trace_data[["seg"]]
        is_y_test_file = False
    
    y_test_save = pd.DataFrame(y_test.astype(int), columns = ["anomaly"])
    y_test_save ["context"] = trace_data[["seg"]]

    num_verbose_printed = 0
    max_verbose_to_show = 20
    show_types=[1]
    results_list = []
    for i in range(y_test.size):
        # counts = Counter(trace_data["EventSequence"].values[i])
        counts = OrderedDict(trace_data["EventSequence"].values[i])
        reason = [{k:v} for (k,v) in counts.items()]
        results_list.append(reason)
        if int(y_test[i]) in show_types:
            num_verbose_printed += 1
            if num_verbose_printed < max_verbose_to_show:
                reason_str = (int(y_test[i]),trace_data["seg"].values[i] , "->", reason)
                print(reason_str)

    y_test_save ["reason"] = results_list
    base_dir=os.path.join(results_directory, "anomalies")
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)
    anomalies_log_file = f'{log_with_possible_errors}(({method["short name"]}))__<<{context_model}>>.csv'
    out_path = os.path.join(base_dir,  anomalies_log_file)
    y_test_save.to_csv(out_path, index=False)

    results = update_results_with(log_with_possible_errors, method["short name"], context_model, y_test, y_test_expect)

    uppdate_meta_results_with("model", context_model, results, is_y_test_file)
    uppdate_meta_results_with("method", method["short name"], results, is_y_test_file)
    uppdate_meta_results_with("combined", f'{context_model} using {method["short name"]}', results, is_y_test_file)
        
    # method["short name"] context_model
    
    # original_name = Path(self.logname).name
    # results_directory+'timing_results.csv'

    # y_testz = np.copy(y_test)

    # y_testz.at[1,'anomaly'] = 1



def scree_plot(model, x_train):
    n_elements_s=model.normal_comonents
    n_elements_e=x_train.shape[1]
    diff = n_elements_e - n_elements_s
    n_elements = int(n_elements_s * 2)
    if n_elements > n_elements_e:
        n_elements = n_elements_e
    # n_elements = n_elements_s
    data = pd.DataFrame(data={"Principal Component":["PC " + str(n + 1) for n in range(n_elements)], "Explained Variance in percent":[100 * model.sigma[n] / model.total_variance for n in range(n_elements)]})

    title = f'Scree Plot - method:{method["friendly name"]}- training data<br>For:"{context_file}"'
    fig = px.bar(data, x='Principal Component', y='Explained Variance in percent', title=title)
    fig.add_vline(x=(n_elements_s - 0.5), line_dash="dash", line_color="red")

    filename = f'Scree-Plot---{method["friendly name"]}---{context_file}'
    # fig.show()
    # fig.write_html(f"{plot_directory}/{filename}.html")
    fig.update_layout(title_font_size=12)
    fig.write_image(f"{plot_directory}/{filename}.png")

def SPE_plot_Markers(model, x_test):
    num = model.get_SPE(x_test)
    df = pd.DataFrame(data=num, columns=['Item No', 'SPE'])
    series = df['SPE']
    indices = [i for i in range(series.shape[0]) if series[i] >= model.threshold] # find_peaks(series, threshold=model.threshold)[0]
    fig = go.Figure()
    title = f'SPE Plot - method: "{method["friendly name"]}<br>"For:"{context_file}"'
    fig.update_layout(title= title,
            font=dict(
            size=18
    ))
    fig.add_trace(go.Scatter(y=series, 
            mode='lines+markers', 
            name='SPE (test data)'))
    fig.add_trace(go.Scatter(y=[model.threshold for i in range(series.shape[0]) ], 
            mode='lines', 
            name='Threshold', line_color = 'orange'))
    fig.add_trace(go.Scatter(
            x=indices, 
            y=[series[j] for j in indices], 
            mode='markers', 
            marker=dict(
                size=8, 
                color='red', 
                symbol='cross'), 
            name='Detected Anomaly'))

    fig.update_yaxes(title="Squared Prediction Error (SPE) ")
    fig.update_xaxes(title="Feature Number")
    
    filename = f'SPE-Plot---{method["friendly name"]}---{context_file}'
    # fig.show()
    # fig.write_html(f"{plot_directory}/filename.html")
    fig.update_layout(title_font_size=12)
    fig.write_image(f"{plot_directory}/{filename}.png", width=1200)





@time_this(friendly_name="Feature Extraction")
def do_generic_feature_extract_data_from(train_data):
    print('- Feature extraction sub-phase (train):')
    x_train_data = train_data["EventSequence"].values
    
    feature_extractor = preprocessing.FeatureExtractor()
    x_train,x_df = feature_extractor.fit_transform(x_train_data)  #feature extractor prepared for later evaluation

    return feature_extractor, x_train


@time_this(friendly_name="Feature Extraction")
def do_pca_feature_extract_data_from(train_data):
    print('- Feature extraction sub-phase (train):')
    x_train_data = train_data["EventSequence"].values
    feature_extractor = preprocessing.FeatureExtractor()
    x_train, x_df = feature_extractor.fit_transform(x_train_data, term_weighting='tf-idf', 
                                              normalization='zero-mean')
    # graph_feature_grid(x_train, x_df)
    # graph_train_grid(x_train, x_df)

    return feature_extractor, x_train


@time_this(friendly_name="Training")
def iForest_model_trained_on(x_train):
    ## --- set up model with training data
    print('- Train...')
    # Initialize iForest
    model = IsolationForest(contamination=anomaly_ratio)

    #this trains the model
    print('- Fit....:')
    model.fit(x_train)
    return model


@time_this(friendly_name="Training")
def iMiner_model_trained_on(x_train):
    ## --- set up model with training data
    print('- Train...')
    # Initialize iMiner
    if (method['short name'] == "use_trace_forks"):
        longest_invarant=3
    else:
        longest_invarant=None
    model = InvariantsMiner(epsilon=epsilon,longest_invarant=longest_invarant)

    #this trains the model
    print('- Fit....:')  
    model.fit(x_train)
    return model


@time_this(friendly_name="Training")
def pca_model_trained_on(x_train):
    ## --- Train the model
    print('- Train...')
    
    # Initialize PCA
    model = PCA() 
    
    #this trains the model
    print('- Fit....:')
    model.fit(x_train)
    return model


@time_this(friendly_name="Complete Run")
def do_pca_run(train_data, data_to_be_tested):
    print('Train phase:')
    feature_extractor, x_train = do_pca_feature_extract_data_from(train_data)
    model = pca_model_trained_on(x_train)

    print('Test phase:')
    x_test, y_test = do_model_evaluation(feature_extractor, model, data_to_be_tested)
# plot_scatter_for(x_test)

# fig = px.scatter(x_test, x=2, y=3)
# fig.show()
# fig = px.scatter(x_test, x=4, y=5)
# fig.show()
    return model, x_test, y_test, x_train





@time_this(friendly_name="Transform Test Data")
def model_transform_test_data(feature_extractor, data_to_be_tested):
    ## --- Use the trained model for anomaly detection on test data
    # Go through the same feature extraction process with training, using transform() instead
    print('- Feature extract (test):')
    test_data = data_to_be_tested["EventSequence"].values
    x_test = feature_extractor.transform(test_data) 
    return x_test

# Calculations
@time_this(friendly_name="Predict anomalies")
def model_Predict(model, x_test):
    ## --- make predictions and alter on anomaly cases
    print('- Predict...')
    y_test = model.predict(x_test)
    return y_test

@time_this(friendly_name="Anomaly Detection")
def do_model_evaluation(feature_extractor, model, data_to_be_tested):
    x_test = model_transform_test_data(feature_extractor, data_to_be_tested) 
    y_test = model_Predict(model, x_test)
    return x_test, y_test





def pca_run(train_data, data_to_be_tested):
    model, x_test, y_test, x_train = do_pca_run(train_data, data_to_be_tested)

    if "do_plots" in globals():
        if not os.path.exists(plot_directory):
            os.mkdir(plot_directory)
        SPE_plot_Markers(model, x_test)
        scree_plot(model, x_train)

    return x_test, y_test
    

@time_this(friendly_name="Complete Run")
def iForest_run(train_data, data_to_be_tested):
    print('Train phase:')
    feature_extractor, x_train = do_generic_feature_extract_data_from(train_data)
    model = iForest_model_trained_on(x_train)
    
    print('Test phase:')
    x_test, y_test = do_model_evaluation(feature_extractor, model, data_to_be_tested)
    
    return y_test


@time_this(friendly_name="Complete Run")
def iMiner_run(train_data, data_to_be_tested):
    print('Train phase:')
    feature_extractor, x_train = do_generic_feature_extract_data_from(train_data)
    model = iMiner_model_trained_on(x_train)
    
    print('Test phase:')
    x_test, y_test = do_model_evaluation(feature_extractor, model, data_to_be_tested)

    return y_test


def run_all_models_using(train_data, data_to_be_tested):
    global context_model

    if "do_iForest" in globals():
        print("\n")
        print("------------------------------")
        print("-------ISOLATION FOREST-------")
        print("------------------------------")
        context_model = "isolation forest"
        y_test = iForest_run(train_data, data_to_be_tested)
        show_predicted_anomalies(y_test, data_to_be_tested)
    
    if ("do_iMiner" in globals()):# and (method['short name'] != "use_trace_forks"):
        print("\n")
        print("------------------------------")
        print("-------INVARIANTS MINER-------")
        print("------------------------------")
        context_model = "Invariants Miner"
        y_test = iMiner_run(train_data, data_to_be_tested)
        show_predicted_anomalies(y_test, data_to_be_tested)

    if "do_pca" in globals():
        print("-----------------")
        print("-------PCA-------")
        print("-----------------")
        context_model = "PCA"
        x_test, y_test = pca_run(train_data, data_to_be_tested)
        show_predicted_anomalies(y_test, data_to_be_tested)


def get_train_data_to_include(train_data, data_to_be_tested):
    keys = set(train_data.keys()) - {data_to_be_tested}
    assert(len(keys) == (len(train_data)-1))
    train_to_include = pd.DataFrame()
    for key in keys:
        train_to_include = train_to_include.append(train_data[key], ignore_index=True)

    shuffled_train_data = train_to_include.sample(frac=1) .reset_index() #drop=True)
    return shuffled_train_data




#--- training log
# train_file_directory = '../../../data/gen/train_structured/structured/'
# train_file_directory = '../../../data/gen/train_structured/structured_good'
# train_file_directory = '../../../data/gen/train_structured/structured_quick'
train_file_directory = '../../../data/gen/train_structured/structured_all'  #20 traces
# train_file_directory = '../../../data/gen/train_structured/structured_everything'  #30 traces
trace_file_directory = train_file_directory
# trace_file_directory = '../../../data/gen/trace_structured/structured/'
# trace_file_directory = '../../../data/gen/trace_structured/trace_structured__gen_error/'
results_directory = '../../../data/results/'
expect_results_dir = '../../../data/expected/'
plot_directory =  os.path.join(results_directory, "plots")

# -- ML fine tuning
#iForest
anomaly_ratio = 'auto' # Estimate the ratio of anomaly samples in the data
#iMiner
epsilon = 0.5 # threshold for estimating invariant space
#debugging
# is_quick_test = True             #uncomment for quicker test
# is_only_test_error_files = True  #uncomment for quicker test
# is_short_method_test = True      #uncomment for quicker test

if "is_quick_test" in globals():
    max_limited_lines = 10000

method_list=[
    {'short name': "use_exec_slice",'friendly name': '100_LoC-Window'},
    {'short name': "use_trace_forks",'friendly name': 'Code-Forks'},
    {'short name': 'use_call_grid','friendly name': 'Call-Grid'},
    ]

if "is_short_method_test" in globals():
    method_list = [method_list[-1]]
#feature method to try

timeslice_size = 100
do_plots = True

# ML models to try
do_pca = True
do_iForest = True
do_iMiner = True



if __name__ == '__main__':
    time_stats=[]
    results_stats = pd.DataFrame()
    meta_stats = OrderedDict()
    
    train_filenames = os.listdir(train_file_directory)
    for method in method_list: 
        context_model = "unspecified"
        context_file = "unspecified"
            ## --- Load strutured log file and extract feature vectors
        print('Load phase:')
    
    
        context_file = "training data"
        print(f'- Loading training data for {method["friendly name"]}: all data in {train_file_directory} ...')
        train_data = load_trining_data(train_file_directory, train_filenames)

        all_test_files = [a for a in train_data.keys()]
        if "is_only_test_error_files" in globals():
            all_test_files = [f for f in all_test_files if "__" in f ]
        all_test_files.sort()
        for log_with_possible_errors in all_test_files:
            context_model = "unspecified"
            context_file = log_with_possible_errors
            print('- Loading test data:', log_with_possible_errors)
            data_to_be_tested = train_data[log_with_possible_errors] #
            
            train_data_to_include = get_train_data_to_include(train_data, log_with_possible_errors)
            run_all_models_using(train_data_to_include, data_to_be_tested)
            print("_______________________________________________________________")

    print("Done")
    
    if len(time_stats) > 0:
        file = open(results_directory+'timing_results.csv', 'w', newline ='')
        with file:
            writer = csv.DictWriter(file, fieldnames = time_stats[0].keys())
            writer.writeheader()
            writer.writerows(time_stats)

    if len(meta_stats) > 0:
        for stat in meta_stats:
            for variant in meta_stats[stat]:
                actual = np.array(meta_stats[stat][variant]["actual"])
                expected = pd.DataFrame(meta_stats[stat][variant]["expected"],columns =['anomaly'])
                if stat == "combined":
                    update_results_with(variant,"_","_", actual, expected)
                elif stat == "method":
                    update_results_with("_",variant,"_", actual, expected)
                elif stat == "model":
                    update_results_with("_","_",variant, actual, expected)

    if len(results_stats) > 0:
        results_stats.to_csv(os.path.join(results_directory,'accuracy_results.csv'), index=False)

